---
title: Priority queue explanation
sidebar_label: Priority queue explanation
---
Priority queue works different in each version, so make sure to look at the correct section for the version you have.

## Free version

The priority queue feature is activated when the player has the `ajqueue.priority` permission. If the player has the permission and they join a queue, they will be sent to the beginning of the queue. If there is another priority player at the beginning of the queue, the plugin will put the 2nd priority player behind the 1st one.

To give players priority queue for a certain server, give them the permission node `ajqueue.serverpriority.<server>`
Make sure to replace `<server>` with the name of the server.

## Plus version

In ajQueuePlus, priority queue works differently. You give priority queue players the permission `ajqueue.priority.<number>` replacing `<number>` with a number. The higher the number, the higher their priority is. If there is Bob with priority 1 and Joe with priority 2 in the queue, Joe will be ahead of Bob in the queue.
To give players priority queue for a certain server, give them the permission `ajqueue.serverpriority.<server>.<number>`. Make sure to replace `<server>` and `<number>` with the correct values (the name of the server for server (case-sensitive) and the priority level for number).
