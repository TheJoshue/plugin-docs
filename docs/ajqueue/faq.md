---
id: faq
title: Frequently Asked Questions
sidebar_label: FAQ
---
These are a few commonly asked questions and their answers. You can use the bar on the right to skip to your question.

---

## How do I set up a queue?
You do not need to set up each server for a queue, queues are automatically created for a server, all you have to do is set player to join them using the queue command.

## Why does the queue not work? I just get sent to the server instantly!
If you join a queue, its empty, and nobody was just sent to the server, then you will get sent instantly. It will not do this when there is more than one person in the queue (or if the server is offline/full/restarting/paused/restricted etc)

## I set my server selector or npc to execute the queue command as the player, but it says unknown command!
This is because its trying to execute the queue command on spigot, when the queue command is on the proxy. If you install ajQueue on the spigot server too (its the same jar), then ajQueue will forward the command to the proxy for you.
