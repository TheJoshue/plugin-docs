---
title: Queue Scoreboard
sidebar_label: Queue Scoreboard
---
It is possible to show a different scoreboard when the player is in a queue.

This is possible using the Queue Scoreboard Activator Event and a scoreboard plugin that
supports event activators

## Known supported plugins
These plugins are known to support event activators. If theres one that supports it that is missing, let me know.
* [KiteBoard](https://www.spigotmc.org/resources/kiteboard.13694/) (I personally use KiteBoard)

*If you know any other plugins that support event activators, let me know*

## Explanation (how it works)
How this works is that the spigot side of ajQueue emits a bukkit event whenever a player joins the queue.
Some scoreboard plugins are able to pick up on the event, and display a scoreboard.

The event is called every 2 seconds while someone is in the queue.

## Setup
In your scoreboard plugin, find how you make scoreboards based on events, and use the event
`us.ajg0702.queue.spigot.QueueScoreboardActivator`. This event is called every 2 seconds while a player is in the queue,
so if there is a timeout option, set it to 2 or 3 seconds.

## Examples
### KiteBoard
This is an example of a scoreboard file with KiteBoard.
```yaml
setting:
  priority: 500
  advanced: false

criteria:
  1:
    type: EVENT
    event: "us.ajg0702.queue.spigot.QueueScoreboardActivator"
    event-player: "getPlayer"
    stay-seconds: 3

content:
  - ''
  - '&6Welcome, &a&l%player_name%'
  - ''
  - "20|&6Queued for:&f %ajqueue_queued%"
  - "20|&7  Position &f%ajqueue_position%&7/&f%ajqueue_of%"
  - ''
  - '20|&6Total players: &a&l%premiumvanish_bungeeplayercount%'
  - ''
  - '&e&lyourserver.com'

```
