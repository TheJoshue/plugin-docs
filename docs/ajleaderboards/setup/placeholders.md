---
id: placeholders
title: Placeholders
sidebar_label: Placeholders
---
These placeholders are for outputting leaderboards.
Do not try to use them in the ajLeaderboards add command!

If you don't know how to use them, check out [the example on holograms on the setup page](/ajleaderboards/setup/setup#3-optional-display-the-leaderboard-in-a-hologram).

**Do not try to use these without first [setting up the plugin](/ajleaderboards/setup/setup)!**

**__Do not forget__** to fill in `<board>` and `<number>` in the placeholders! They will not work without those.
`<board>` is the name of your leaderboard (for example, `statistic_player_kills`), and
`<number>` is the position you want to display (1 for 1st, 2 for 2nd, etc)
`<type>` is the timed type (e.g. alltime, hourly, daily, weekly, monthly)

### v2
ajLeaderboards v2.0.0 or newer.

| Placeholder | Description |
| ------ | ------ |
| `%ajlb_position_<board>_<type>%` | Shows what position the player is at in the leaderboard `<board>`  |
| `%ajlb_value_<board>_<type>%` | Shows the user's value for `<board>` |
| `%ajlb_value_<board>_<type>_formatted%` | Shows the user's value for `<board>`, and formatted |
| `%ajlb_value_<board>_<type>_time%` | Show the user's value for `<board>`, and formatted for time |
| `%ajlb_value_<board>_<type>_raw%` | Show the user's raw value for `<board>`. No formatting at all. (including commas) |
| `%ajlb_lb_<board>_<number>_<type>_name%` | Shows the name of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_<type>_value%` | Shows the score of the player in `<number>` place on leaderboard `<board>`  |
| `%ajlb_lb_<board>_<number>_<type>_time%` | Shows the time of the player in `<number>` place on leaderboard `<board>`. Formatted like `2w 3d 17h 14m`  |
| `%ajlb_lb_<board>_<number>_<type>_rawvalue%` | Shows the score of the player in `<number>` place on leaderboard `<board>`, but without commas  |
| `%ajlb_lb_<board>_<number>_<type>_value_formatted%` | Shows the formatted score of the player in `<number>` place on leaderboard `<board>`. Example: 10000 shows as 10k  |
| `%ajlb_lb_<board>_<number>_<type>_prefix%` | Shows the vault prefix of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_<type>_suffix%` | Shows the vault suffix of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_<type>_color%` | Shows the color of the players name. It is basically the prefix but only the color codes. |


### v1
ajleaderboards v1.x.x is deprecated. You will get no support with 1.x.x.
<details><summary>See placeholders anyway</summary>

| Placeholder | Description |
| ------ | ------ |
| `%ajlb_position_<board>%` | Shows what position the player is at in the leaderboard `<board>`  |
| `%ajlb_value_<board>%` | Shows the user's value for `<board>` |
| `%ajlb_value_<board>_formatted%` | Shows the user's value for `<board>`, and formatted |
| `%ajlb_lb_<board>_<number>_name%` | Shows the name of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_value%` | Shows the score of the player in `<number>` place on leaderboard `<board>`  |
| `%ajlb_lb_<board>_<number>_rawvalue%` | Shows the score of the player in `<number>` place on leaderboard `<board>`, but without commas  |
| `%ajlb_lb_<board>_<number>_value_formatted%` | Shows the formatted score of the player in `<number>` place on leaderboard `<board>`. Example: 10000 shows as 10k  |
| `%ajlb_lb_<board>_<number>_prefix%` | Shows the vault prefix of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_suffix%` | Shows the vault suffix of the player in `<number>` place on leaderboard `<board>` |
| `%ajlb_lb_<board>_<number>_color%` | Shows the color of the players name. It is basically the prefix but only the color codes. |

</details>
