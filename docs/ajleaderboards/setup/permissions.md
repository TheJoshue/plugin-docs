---
id: permissions
title: Permissions
sidebar_label: Permissions
---
Here is a list of permissions for the plugin.

| Permission | Description |
| --- | --- |
| `ajleaderboards.use` | Permission to use commands in /ajleaderboards |
| `ajleaderboards.dontupdate.<board>` | Any players with this permission will not be updated on the leaderboard |
