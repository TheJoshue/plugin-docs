---
id: faq
title: Frequently Asked Questions
sidebar_label: FAQ
---
These are a few commonly asked questions and their answers. You can use the bar on the right to skip to your question.

---

## Why does the leaderboard only show online players?
**ajLeaderboards will show offline players!**

ajLeaderboards only adds players to the leaderboard once they've joined at least once since setting up the leaderboard. That means that players will have to join once after you set up the leaderboard in order to show up on it. Once they are added to the leaderboard, they will stay on the leaderboard (even if they leave) unless you manually remove them.

You can attempt to add players manually using `/ajlb update <board> <player>`, but not all plugins support this. If it doesn't work, you should ask the developer of the plugin that has the placeholder to add support for offline player placeholders.

## How can I show the player's prefix / rank on the leaderboard?
ajLeaderboards can display the player's prefix on the leaderboard even when they are offline.

Make sure you have [vault](https://www.spigotmc.org/resources/vault.34315/) installed on your server, then you can put the [ajLeaderboards prefix placeholder](/ajleaderboards/setup/placeholders) infront of the name placeholder in order to show their prefix

## I've added signs, but they just say `board not found!`
Make sure you typed the board name correctly. It should tab complete in the command, so double check with that and also in `/ajlb list`

If the placeholder you are trying to use does not show up in either of those, please follow the [setup guide](/ajleaderboards/setup/setup)

## The placeholders don't work on holograms!
This is a common issue if you use HolographicDisplays.

You need to install HolographicExtension and ProtocolLib. If you are missing one of these plugins, placeholderapi placeholders will not work on holograms.

You can also skip HolographicExtension if you use HolographicDisplays v3.0.0 or newer. (you will still need ProtocolLib)

If you are using a different hologram plugin, check that plugin's plugin page and wiki for how to use placeholderapi placeholders.

## My server is lagging with ajLeaderboards!
If you are using head blocks, this is a fairly common issue. The way the spigot api works, there is no way to make heads *not* laggy without very careful nms code, which would require an update for every minecraft version. An alternative to head blocks is to use heads on armor stands, which should not cause lag.

## All of the positions in the leaderboard have the same value (but different players)
Make sure you read where it says ⚠️Important⚠️ in [step 1 in the setup guide](/ajleaderboards/setup/setup#1-figure-out-which-placeholder-to-use)
