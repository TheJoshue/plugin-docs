Blocks.yml is a file that you can list all of the blocks you want to have for parkour.

## Material list
brc made a nice material list for each version [here](https://docs.brcdev.net/#/materials).

## <1.12 data values
To use data values for mc versions 1.12 and below, you can simply use a colon (:) to seperate the name and the data value. Example:
```yaml
- WOOL:6
```

## Per-area blocks
**Requires v2.5.0 or later**

If you want to have certain blocks only be randomly selected for certain areas, you can.
simply put a semi-colon (;) at the end then put area names seperated by commas. Example:
```yaml
- GRASS_BLOCK;spawn
```
This grass block will only be randomly selected for the area named `spawn`.
```yaml
- stone;caves,spawn
```
This stone block will only be randomly selected for the areas `caves` and `spawn`.
