---
title: Flags
sidebar_label: Flags
---
You can have special flags on commands that can make the commands only execute when certain things happen. For example, if you want to delay the command by 5 seconds, you can use the `d=5;` flag.

---

## Start command flags
There is currently only one flag that is compatible with the start commands: the delay flag.

### Delay flag
To use the delay flag, simply add `d=<number>;` to the beginning of the command, replacing the `<number>` with the number of seconds to delay the command. For example this: `d=5;say hello!` will execute the command `say hello!` 5 seconds after the server finishes loading.

---

## Join command flags
Join commands can have several flags: delay, permission, first join, and execute as player.

### Delay flag
To use the delay flag, simply add `d=<number>;` to the beginning of the command, replacing the `<number>` with the number of seconds to delay the command. For example this: `d=5;say hello!` will execute the command `say hello!` 5 seconds after the player joins.

### Permission flag
The permission flag will only execute the command if the player has a certain permission. To use it, just add `perm=<permission>;` to the beginning of the command, replacing `<permission>` with the permission node you want to check for. For example, this: `perm=join.creative;gamemode creative {PLAYER}` will put the player in creative mode when they join if they have the permission `join.creative`.

### First join flag
The first join flag will only execute a command if this is the player's first time joining. To use it, simply add `[fj]` to the beginning of the command.

### Execute as player flag
The execute as player flag will make the command execute as the player instead of the console. To use this flag, simple add `[p]` to the beginning of the command.

---
## Using multiple flags
If you want to use multiple flags in the player join commands, they must be in a certain order: delay, permission, first join, then execute as player. For example, if you had all the flags on a command it would look like this: `d=5;perm=join.hi;[fj][p]say hi!`.
