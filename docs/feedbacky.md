---
id: feedbacky
title: Feedbacky
sidebar_label: Feedbacky
slug: /feedbacky
---
If you would like to make a suggestion for one of my plugins, feedbacky is the best place to do that.

Feedbacky is a website that is built from the ground up to track suggestions for projects.

[ajQueue](https://app.feedbacky.net/b/ajQueue)

[ajParkour](https://app.feedbacky.net/b/ajparkour)

[ajLeaderboards](https://app.feedbacky.net/b/ajLeaderboards)

[ajTNTRun](https://app.feedbacky.net/b/ajtntrun)
