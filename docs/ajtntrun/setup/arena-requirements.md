---
title: Arena Requirements
sidebar_label: Arena Requirements
---
An arena must fit a few requirements in order to be able to be used by this plugin:
*  Must have any block on top of TNT. (see image below)
*  Can have pressure plates on top
*  Should not be accessible to players not teleported in by the game.


(The pressure plates are optional)
![2019-06-23_11.11.51](uploads/32018e15a842ebd78f983e1b5e6040a2/2019-06-23_11.11.51.png)
