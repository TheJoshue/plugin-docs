---
title: Setting a lobby
sidebar_label: Setting a lobby
---
In order to play games, you must set the main lobby.

The main lobby is the place players are teleported when the game ends, they leave, or any other reason they need to be teleported out of the arena. If the server is in bungee mode, players are teleported to this location while it attempts to connect you to the specified bungeecord server.

To set it, stand where you would like for the teleport location to be, and type `/tntrun setlobby`. This will also reload all arenas and the config.
